import _ from 'underscore';
import $ from 'jquery';

const envBarOptions = {};
const update = function(mUserCurrent) {
    !mUserCurrent || (envBarOptions.mUserCurrent);

    $('#_environment-bar-content').html(
        'env: ' + envBarOptions.env_name + ' &nbsp; &nbsp; ' +
        'version: ' + envBarOptions.version + ' &nbsp; &nbsp; ' +
        'debug: ' + envBarOptions.debug + ' &nbsp; &nbsp; ' +
        'user: ' + (mUserCurrent.isLoggedIn() ? mUserCurrent.get('id') + '/' + mUserCurrent.get('username') : 'null') + ' &nbsp; &nbsp; ' +
        'csrf_token: ' + envBarOptions.csrf_token
    );
};
const setup = function(options = {}) {
    _.extend(envBarOptions, options);

    $('#_environment-bar').attr('style', envBarOptions.style);
    $('#_environment-bar-movetop').click(() => {
        $('#_environment-bar').css('transform', 'rotate(0) translate(0, 0)');
        $('#_environment-bar-movetop').hide();
        $('#_environment-bar-moveside').show();
    });
    $('#_environment-bar-moveside').click(() => {
        $('#_environment-bar').css('transform', 'rotate(90deg) translate(100%, 0)');
        $('#_environment-bar-moveside').hide();
        $('#_environment-bar-movetop').show();
    });

    update(options.mUserCurrent);
};

export { setup as setup };
export { update as update };
