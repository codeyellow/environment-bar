# environment-bar

Show an environment bar.

## Usage

Import as follows:

    import * as environmentBar from 'environment-bar';

Somewhere in your application initializer:

    app.addInitializer(() => {
        environmentBar.setup({
            mUserCurrent: mUserCurrent,
            style: config.ENV_BAR_CSS
        });
    }); 

Update environment bar upon login/logout:

    app.listenTo(vent, 'after:user:login:success', environmentBar.update);
    app.listenTo(vent, 'after:user:logout:success', environmentBar.update);

The `update` function expects an `mUserCurrent` as argument. If not given, it will fall back to the one defined during setup.